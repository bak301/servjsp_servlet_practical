<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 24/05/2018
  Time: 11:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>RIP</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <header>
        <h1>Create an Account</h1>
        <h3>Your registration information</h3>
        <p>Please provide your contact info below. When you are finished, simply click the Continue button</p>
      </header>

      <main>
        <form action="/register" method="post">
          <div class="form-group">
            <label for="firstName">First Name : </label>
            <input type="text" id="firstName" name="firstName" class="form-control"/>
          </div>

          <div class="form-group">
            <label for="lastName">Last Name : </label>
            <input type="text" id="lastName" name="lastName" class="form-control"/>
          </div>

          <div class="form-group">
            <label for="dob">Day of Birth : </label>
            <input type="date" id="dob" name="dob" class="form-control"/>
          </div>

          <div class="form-group">
            <label for="email">Email : </label>
            <input type="email" id="email" name="email" class="form-control"/>
            <label><input type="checkbox">Please send me info by e-mail about future Army opportunities</label>
          </div>

          <div class="form-group">
            <label for="zipCode">Zip Code : </label>
            <input type="number" id="zipCode" name="zipCode" class="form-control"/>
          </div>

          <input type="submit" class="btn btn-primary" value="Continue"/>
        </form>
      </main>
    </div>
  </body>
</html>
