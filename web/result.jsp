<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 24/05/2018
  Time: 12:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Result Page</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <header>
            <h1>User registered !</h1>
        </header>

        <main>
            <c:set value="${requestScope.bean.getUser()}" var="user"/>
            <p>First name : ${user.getFirstName()}</p>
            <p>Last Name : ${user.getLastName()}</p>
            <p>Day of Birth : ${user.getDob()}</p>
            <p>Email : ${user.getEmail()}</p>
            <p>Zip Code : ${user.getZipCode()}</p>
        </main>
    </div>
</body>
</html>
