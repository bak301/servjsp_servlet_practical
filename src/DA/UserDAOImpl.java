package DA;

import entity.User;
import org.mariadb.jdbc.MariaDbConnection;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UserDAOImpl implements UserDAO {
    private MariaDbConnection con;

    public UserDAOImpl() {
        this.con = DBConnect.getConnection();
    }

    @Override
    public boolean addUser(User user) {
        if (con != null) {
            try {
                String sql = "INSERT into user(firstname, lastname, dob, email, zipcode)" +
                        " values (?,?,?,?,?);";
                PreparedStatement stm = con.prepareStatement(sql);
                stm.setString(1, user.getFirstName());
                stm.setString(2, user.getLastName());
                stm.setDate(3, java.sql.Date.valueOf(user.getDob()));
                stm.setString(4, user.getEmail());
                stm.setString(5, user.getZipCode());

                return stm.executeUpdate() > 0;
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return false;
    }
}
