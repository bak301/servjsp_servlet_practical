package model;

import DA.UserDAO;
import DA.UserDAOImpl;
import DA.UserDAOImplNODB;
import entity.User;

import java.io.Serializable;

public class UserBean implements Serializable {
    private User user;
    private UserDAO dao;

    public UserBean() {
//        dao = new UserDAOImpl(); Use this when we got database;
        dao = new UserDAOImplNODB();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean register() {
        return dao.addUser(user);
    }
}
