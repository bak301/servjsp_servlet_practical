package controller;

import entity.User;
import model.UserBean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

public class RegistrationController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserBean bean = new UserBean();

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        LocalDate dob = LocalDate.parse(request.getParameter("dob"));
        String email = request.getParameter("email");
        String zipCode = request.getParameter("zipCode");

        User user = new User(firstName, lastName, dob, email, zipCode);
        bean.setUser(user);
        boolean isRegisterSuccess =  bean.register();

        request.setAttribute("bean", bean);
        request.getRequestDispatcher(isRegisterSuccess ? "result.jsp" : "error.jsp").forward(request, response);
    }
}
